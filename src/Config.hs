module Config where

import Data.Int (Int64)

maxRequestBodyLength :: Int64
maxRequestBodyLength = 2048
